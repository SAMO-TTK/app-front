const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const { autoUpdater } = require("electron-updater");

const url = process.env.DEV === "true" ? 'http://localhost:4000' : `file:///${__dirname}/dist/index.html`;
if (process.env.DEV === "true") {
    process.env.NODE_ENV = 'development';
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function initializeApp() {

    // Create the browser window.
    mainWindow = new BrowserWindow({
        width         : 1920,
        height        : 1017,
        fullscreen    : false,
        webPreferences: {
            webSecurity: false
        },
        // frame: process.env.CLIENT == "true"
    });
    mainWindow.loadURL(url);
    if (process.env.CLIENT !== "true") {
        mainWindow.webContents.openDevTools();
    }
    mainWindow.on('closed', function () {
        mainWindow = null;
    });
	let popWindow = new BrowserWindow({
		parent       :mainWindow,
		width         : 1600,
		height        : 1200,
		show: false,
	});

	/* wait for a response https://github.com/electron-userland/electron-builder/issues/3259 */
   /*if (process.env.CLIENT === "true") {
        autoUpdater.setFeedURL({
            provider: "generic",
            url:'http://api-ttk.dev.itineraire-b.com/application/client'
        });
    }
    else {
        autoUpdater.setFeedURL({
            provider: "generic",
            url:'http://api-ttk.dev.itineraire-b.com/application/interne'
        });
    }*/
    //console.log(autoUpdater.getFeedURL());

    autoUpdater.checkForUpdatesAndNotify().then(response => {
        //console.log(response);
    });

    autoUpdater.on('update-downloaded', (ev, info) => {
        autoUpdater.quitAndInstall();
    });
}


app.on('ready', initializeApp);

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', function () {
    if (mainWindow === null) {
        initializeApp();
    }
});
