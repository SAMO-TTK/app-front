const webpack = require('webpack');
const path = require('path');

const IS_ELECTRON = process.env.IS_ELECTRON && process.env.IS_ELECTRON.toString().toLowerCase() === 'true';
const VERSION = require('./package.json').version;

let apis;
if (process.env.CLIENT === "true") {
    apis = {
        API:'http://192.168.2.11/api',
        STORAGE : 'http://192.168.2.11/storage',
        WANA : 'http://wkrest.toutenkamion.com/',
    }
}
else {
    apis = {
        API: 'http://api-ttk.dev.itineraire-b.com/api',
        STORAGE : 'http://api-ttk.dev.itineraire-b.com/storage',
        WANA : 'http://wkrest.toutenkamion.com:81'
    }
}

module.exports = {
    //#region Dev server conf
    devServer: {
        disableHostCheck: true,
        proxy: {
            '/storage': {
                target      : 'http://ttkhub.dev2.itineraireb.com/storage',
                changeOrigin: true,
                pathRewrite : {
                    '^/storage': ''
                }
            },
            '/api': {
                target      : 'http://ttkhub.dev2.itineraireb.com/api',
                changeOrigin: true,
                pathRewrite : {
                    '^/api': ''
                }
            },
			'/mfiles': {
				target      : 'https://mfiles.toutenkamion.com/REST',
				changeOrigin: true,
				pathRewrite : {
					'^/mfiles': ''
				}
			},
			'/viewmfiles': {
				target      : 'https://mfiles.toutenkamion.com/Default.aspx?#7061608C-319D-43C7-9017-6D940C3731C7/object',
				changeOrigin: true,
				pathRewrite : {
					'^/viewmfiles': ''
				}
			}
        },
    },
    //#endregion

    //#region Electron Conf
    webpack(config) {
        const electronConfig = {
            target: IS_ELECTRON ? 'electron-renderer' : 'web',
            node: {
                fs: 'empty',
                __dirname: process.env.NODE_ENV !== 'production',
                __filename: process.env.NODE_ENV !== 'production'
            },
            externals: {
                'nfc-pcsc': IS_ELECTRON ? "require('nfc-pcsc')" : "{}",
            }
        };

        return Object.assign({}, config, electronConfig);
    },
    html: {
        template: path.resolve(__dirname, './src/index.html'),
        nodeModules: process.env.NODE_ENV !== 'production' && IS_ELECTRON
            ? path.resolve(__dirname, './node_modules')
            : false
    },
    env: {
        ...apis,
        IS_ELECTRON: IS_ELECTRON,
        MFILES : 'https://mfiles.toutenkamion.com/REST',
        VIEWMFILES : 'https://mfiles.toutenkamion.com/Default.aspx?#7061608C-319D-43C7-9017-6D940C3731C7/object',
        NFC: true,
        VERSION
    },

    homepage: './',
    //#endregion

    //#region Performance tweaks
    autoprefixer: false,
    babel:{
        babelrc: true,
        cacheDirectory: true,
    },
    //#endregion

    //#region Desactivated for performance issues
    sourceMap: false,// might want to activate it on certain bugs.
    //#endregion
};
