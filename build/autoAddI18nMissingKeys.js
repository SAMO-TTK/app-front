const watchr = require('watchr');
const vfs = require('vinyl-fs');
const { join } = require('path');
const scanner = require('i18next-scanner');
const sort = require('gulp-sort');

const srcPath = join(__dirname, '../src');
const tradFilesPath = join(srcPath, '/i18n/{{lng}}/{{ns}}.json');

function parseDir() {
    
    const options = {
        removeUnusedKeys: false,
        lngs            : [ 'fr', 'en' ],
        func            : {
            list      : [ 'i18next.t', 'i18n.t', '\\$t' ],
            extensions: [ '.js', '.vue' ]
        },
        resource        : {
            loadPath: tradFilesPath,
        },
    };
    vfs.src([ join(__dirname, '../src/**/*.vue'), join(__dirname, '../src/**/*.js') ])
        .pipe(sort()) // Sort files in stream by path
        .pipe(scanner(options))
        .pipe(vfs.dest(srcPath));
}

parseDir();

const stalker = watchr.create(srcPath);
stalker.setConfig({
    ignorePaths: [ join(srcPath, 'i18n') ]
});

stalker.watch(() => {});

stalker.on('change', parseDir);