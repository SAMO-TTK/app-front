const blessed = require('blessed');
const spawn = require('child_process').spawn;

const defaultConfiguration = {
    cursor     : 'block',
    cursorBlink: true,
    screenKeys : false,
    border     : 'line',
    tags       : true,
    style      : {
        fg   : 'default',
        bg   : 'default',
        focus: {
            border: {
                fg: 'green'
            }
        }
    },
    keys       : true,
    scrollback : 1,
    scrollbar  : {
        ch   : ' ',
        track: {
            bg: 'yellow'
        },
        style: {
            inverse: true
        }
    }
};

function defaultBlessedConfig(override) {
    return Object.assign({}, defaultConfiguration, override);
}

function buildLogger(override, cmd) {
    const logger = blessed.log(defaultBlessedConfig(override));
    const [ app, ...params ] = cmd.split(' ');
    const cmdProcess = spawn(app, params);

    cmdProcess.stdout.on('data', (data) => logger.log(data.toString()));
    cmdProcess.stderr.on('data', (data) => logger.log(`{red-fg}{green-bg}${data.toString()}{/}`));
    logger.__process = cmdProcess;
    return logger;
}

module.exports = { defaultBlessedConfig, buildLogger };
