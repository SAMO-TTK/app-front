const blessed = require('blessed');
const path = require('path');
const { buildLogger } = require('./simpleBuild/helpers.js');

const loggers = [];
function buildLoggerAndKeepTrack(...args) {
    const logger = buildLogger(...args);
    loggers.push(logger);
    return logger;
}

const screen = blessed.screen({
    smartCSR: true
});

const poiAppLogger = buildLoggerAndKeepTrack({
    parent: screen,
    label : 'Poi App BUild',
    left  : 0,
    top   : 0,
    width : '50%',
    height: '100%',
}, 'npm run dev-app');

poiAppLogger.focus();

const elementUILogger = buildLoggerAndKeepTrack({
    parent: screen,
    label : 'Element UI Theme',
    right : 0,
    bottom   : 0,
    width : '50%',
    height: '100%',
}, "npm run dev-theme");

process.on('exit', () => {
    loggers
        .map(logger => logger.__process)
        .forEach(p => {
            p.kill();
        });
});

screen.key([ 'escape', 'q', 'C-c' ], () => {
    return process.exit();
});

let current = 0;
screen.key([ 'tab' ], function (ch, key) {
    current = (current + 1) % loggers.length;
    loggers[ current ].focus();
});
