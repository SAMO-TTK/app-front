import Vue from 'vue';
import store from './kamion/index'

export const mfilesTypes = Object.freeze({
    SET_MFILES   : 'MFILES_SET',

});

const defaultState = {
    mfilesItems: new Map(),
};

const state = defaultState;

const getters = {
    mfilesItems   : state => state.mfilesItems,
    mfilesItemsArray   : state => state.mfilesItems ? [...state.mfilesItems.values()] : [],
};

const actions = {
    setMFiles({commit}) {
		console.log('in mfiles, ==>', store)
        Vue.http.get(`${process.env.MFILES}/valuelists/101/items`)
            .then((response) => {
                const mfilesItemsResponse = response.body.Items.map((item) => {
                    return {
                        id     : item.ID,
                        name   : item.Name,
                        ownerId: item.OwnerID
                    };
                });
                return Promise.all([mfilesItemsResponse, Vue.http.get(`${process.env.MFILES}/valuelists/102/items`)]);
            }).then(([mfilesItemsResponse, response]) => {

                const tree = new Map();

                mfilesItemsResponse.forEach((parent) => {
                    parent.subpaths = new Map();
                    tree.set(parent.id, parent);
                });

                const mfilesSubItemsResponse = response.body.Items;

                mfilesSubItemsResponse.map((item) => {
                    return {
                        id     : item.ID,
                        name   : item.Name,
                        ownerId: item.OwnerID
                    };
                }).forEach((child) => {
                    const parent = tree.get(child.ownerId);
                    if (parent) {
                        parent.subpaths.set(child.id, child);
                    }
                });

                commit(mfilesTypes.SET_MFILES, {mfilesItems: tree});

        }).catch(error => console.error('Unexpected error', error));
    },

};

const mutations = {
    [mfilesTypes.SET_MFILES ](state, {mfilesItems}) {
        state.mfilesItems = mfilesItems;
    },

};

export const mfiles = {
    state,
    getters,
    actions,
    mutations,
};
