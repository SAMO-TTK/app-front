import Vue from 'vue';

const defaultState = {
    isPanelOpen: false,
    isMessagePanelOpen: false,
    discussionsData: [],
    messagesData: [],
    selectedDiscussionId: null
};

const state = defaultState;

const mutations = {
    setDebatePanelState(state, value) {
        state.isPanelOpen = value;
    },
    setDiscussionsData(state, value) {
        state.discussionsData = value;
    },
    setMessagePanelState(state, value) {
        state.isMessagePanelOpen = value;
    },
    setMessagesData(state, value) {
        state.messagesData = value;
    },
    setSelectedDiscussionId(state, value) {
        state.selectedDiscussionId = value;
    }
};

// const getters = {
//     getDiscussionsData: state => state.discussionsData
// };

export const debate = {
    state,
    mutations,
    // getters
};
