import Vue from 'vue';

export const kamionTypes = Object.freeze({
	SET_KAMION: 'KAMION_SET',
	UNSET_KAMION: 'KAMION_UNSET',
	SET_ALLELEMENTS: 'ALLELEMENTS_SET',
	SET_MFILES_ID: 'MFILES_ID_SET',
	SET_MFILES_NAME: 'MFILES_NAME_SET',
	SET_MFILES_PHASE: 'MFILES_PHASE_SET',


});

const defaultState = {
	selected: null,
	chapters: null,
	allElements: null,
	mfilesId: null,
	phase: null,
	mfilesName: null,
};

const state = defaultState;

const getters = {
	selectedKamionId: state => state.selected && state.selected.id,
	selectedKamion: state => state.selected,
	hasKamionSelected: state => state.selected !== null,
	mfilesId: state => state.mfilesId,
	mfilesName: state => state.mfilesName,
	phase: state => state.phase,
};

const actions = {
	setKamion({commit}, {idKamion}) {
		Vue.http.get(`${process.env.API}/kamions/${idKamion}?embed=users,stage,column,project_managers`)
				.then((response) => {
					const kamion = response.body.data;
					commit(kamionTypes.SET_KAMION, {kamion});
					if (localStorage.mfiles != 'null' && localStorage.mfiles != null) {
						Vue.http.get(`${process.env.MFILES}/objects?p1047=${kamion.wana_id}`).then(response => {
							const mfilesId = response.body.Items[0].ObjVer.ID;
							const mfilesName = response.body.Items[0].DisplayID;
							Vue.http.get(`${process.env.MFILES}/objects/106/${mfilesId}/latest/properties/39`).then(rep => {
								let phase = 1;
								switch (parseInt(rep.body.Value.DisplayValue)){
									case 0:
										phase = 1;
										break;
									case 1:
										phase = 2;
										break;
									case 2:
										phase = 3;
										break;
									case 3:
										phase = 4;
										break;
									case 4:
										phase = 5;
										break;
									case 100:
										phase = 6;
										break;
									case 11:
										phase = 7;
										break;
								}
								console.log(rep.body, 'phase',parseInt(rep.body.Value.DisplayValue), phase)
								Vue.http.put(`${process.env.API}/kamions/${idKamion}`, {stage_id: phase});
								const mfilesphase = rep.body.Value.DisplayValue
								commit(kamionTypes.SET_MFILES_PHASE, {mfilesphase})

							})
							commit(kamionTypes.SET_MFILES_ID, {mfilesId});
							commit(kamionTypes.SET_MFILES_NAME, {mfilesName});
						});
					}
				}).catch(error => console.error('Unexpected error', error));
	},


	unsetKamion({commit}) {
		commit(kamionTypes.UNSET_KAMION);
	},
};

const mutations = {
	[kamionTypes.SET_KAMION](state, {kamion}) {
		state.selected = kamion;
	},
	[kamionTypes.UNSET_KAMION]() {
		state.selected = null;
	},
	[kamionTypes.SET_MFILES_ID](state, {mfilesId}) {
		state.mfilesId = mfilesId;
	},
	[kamionTypes.SET_MFILES_NAME](state, {mfilesName}) {
		state.mfilesName = mfilesName;
	},
	[kamionTypes.SET_MFILES_PHASE](state, {mfilesphase}) {
		state.phase = mfilesphase;
		console.log(state)
	},
};

export const kamion = {
	state,
	getters,
	actions,
	mutations,
};
