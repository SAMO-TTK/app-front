import faker from 'faker';

export function generateFakerDocuments() {
    return Array(8).fill(1).map((_, i) => {
        return {
            title       : 'document ' + (i + 1),
            extension       : faker.lorem.word(),
            updated_at: faker.date.past().toLocaleDateString(),
            projectName: faker.lorem.word(),
        };
    });
}

export function generateFakerPathOfExpectetion() {
    return Array(8).fill(1).map((_, i) => {
        return {
            title       : 'chemin des attentes ' + (i + 1),
            updated_at: faker.date.past().toLocaleDateString(),
            projectName: faker.lorem.word(),

        };
    });
}

export function generateFakerNote() {
    return Array(8).fill(1).map((_, i) => {
        return {
            title      : 'Note ' + (i + 1),
            updated_at: faker.date.past().toLocaleDateString(),
            projectName: faker.lorem.word(),

        };
    });
}

export function generateFakerTickets() {
    return Array(8).fill(1).map((_, i) => {
        return {
            title       : 'tickets ' + (i + 1),
            ticket_type     : faker.lorem.word(),
            updated_at: faker.date.past().toLocaleDateString(),
            projectName: faker.lorem.word(),

        };
    });
}

export function generateMultiProjects() {
    return Array(2).fill(1).map((_, i) => {
        return {
            projectName: faker.lorem.sentence(),
            data       : [
                generateFakerPathOfExpectetion(),
                generateFakerDocuments(),
                generateFakerNote(),
                generateFakerTickets(),
            ]
        }
    });

}
