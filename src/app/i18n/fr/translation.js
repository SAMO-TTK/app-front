export default {
    app: {
        bar: {
            settings: 'Paramètres',
            logout: 'Déconnexion',
        },
        search_modal: {
            all_project: 'Rechercher tout projets confondus'
        },
        list: {
            actions: {
                add: '+'
            }
        },
        pathOfExpectetion: {
            aside: {
                mainTitle: 'Chemin des attentes',
                allElementLink: 'Tous les éléments',
                subAside: {
                    title: 'Section',
                    search: 'recherche'
                }
            }
        },
        kamion: {
            information: {
                label: {
                    number: 'Nombre',
                    MPcoeff: 'MP Coeff',
                    hourlyRate: 'Taux horaire',
                    length: 'Longeur',
                    delivery: 'Date de livraison',
                    frameDelivery: 'Date de livraison du châssis',
                    updatedAt: 'Dernière mise à jour',
                    createdAt: 'Date de création',
                    pojectManager: 'Chef de kamion',
                    kamionManager: 'Chef de Kamion',
					workedTime: 'Heures réalisées',
					plannedTime: 'Heures prévue',
                },
                button: {
                    actions: 'Actions'
                },
                delivered: 'livré',
                keyDate: 'Dates clés'
            }
        },
        dashboard: {
            maintTitle: 'Mon tableau',
            lastAnnouncement: 'Dernière annonce',
            documents: {
                title: 'Documents'
            },
            exchange: {
                title: 'Echange',
            },
            pathOfExpectetion: {
                title: 'chemin des attentes',
                lastElements: 'Mes derniers éléments',
                lastDebates: 'Mes derniers débats',
            },
            button: {
                addDocument: 'Ajouter document',
                add: 'ajouter',
                see: 'voir'

            }
        },
        exchange: {
            note: {
                mainTitle: 'Notes'
            }
        },

    },
    list: {
        counter_label: 'résultats'
    },

};
