export default {
    app: {
        bar              : {
            settings: 'Settings',
            logout  : 'Logout',
            search  : 'Search'
        },
        search_modal     : {
            all_project: 'Search for all projects'
        },
        pathOfExpectetion: {
            aside: {
                mainTitle     : 'Path of expectetion',
                allElementLink: 'All elements',
                subAside      : {
                    title : 'Section',
                    search: 'search'
                }

            }
        },
        kamion           : {
            information: {
                label    : {
                    number       : 'Number',
                    MPcoeff      : 'MP Coeff',
                    hourlyRate   : 'Hourly rate',
                    length       : 'length',
                    delivery     : 'Delivery date',
                    frameDelivery: 'Frame Delivery Date',
                    updatedAt    : 'Last update',
                    createdAt    : 'Created date',
                    pojectManager: 'Project manager',
                    kamionManager: 'Kamion manager',
                    workedTime   : 'Worked time',
                },
                button   : {
                    actions: 'Actions'
                },
                delivered: 'Delivered',
                keyDate  : 'Key date'


            }
        },
        dashboard        : {
            maintTitle       : 'My dashboard',
            lastAnnouncement: 'last announcement',
            documents        : {
                title: 'Documents'
            },
            exchange         : {
                title: 'Exchange',
            },
            pathOfExpectetion: {
                title       : 'path of expectetion',
                lastElements: 'Last elements',
                lastDebates : 'Last debates',
            },
            button :{
                addDocument: 'Ajouter document',
                add: 'ajouter',
                see: 'voir'
            }
        }
    },
};
