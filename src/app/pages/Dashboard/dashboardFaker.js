import faker from "faker";

export function generateFakerDatadashboardDocArray() {
    return Array(5).fill(1).map((_, i) => {
        return {
            name: "Projet " + (i+1),
            creationDate: faker.date.past().toLocaleDateString(),
            author: faker.name.findName(),
            type: faker.lorem.word(),
            lastOpeningDate: faker.date.past(),

        }
    })
}
export function generateFakerDatadashboardArray() {
    return Array(5).fill(1).map((_, i) => {
        return {
            title: "Projet " + (i+1),
            type: faker.lorem.word(),
            creationDate: faker.date.past().toLocaleDateString(),
            author: faker.name.findName(),
            confidentiality: faker.date.past(),

        }
    })
}
