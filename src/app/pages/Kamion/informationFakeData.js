import faker from "faker"

export function generateFakerExpectation() {
    return Array(20).fill(1).map((_, i) => {
        return {
            instructionNumber: faker.random.number(),
            kme: faker.random.number(),
            metier: faker.random.word(),
            ressources: 0,
            description: faker.lorem.sentence()
        }
    })
}
 export function generateFakerInformationKamion(){
    return{
        picture: faker.image.image(),
        name: 'BTC DON DU SANG 3 LITS JEDDAH (BRDU) - XXXXX',
        type: 'Transfusion',
        order: 'Commande',
        is_delivered: true,
        kamionManager: faker.name.findName(),
        projectManager: faker.name.findName(),
        labelNumber: 'N°',
        serialNumber: faker.random.number(),
        date: faker.date.between(),
        length: faker.random.number(),
        number: faker.random.number(),
        delivery: faker.date.future(),
        frame_delivery: faker.date.future(),
        hourly_rate: faker.random.number(),
        coeff_mp: faker.random.number(),
        updated_at: faker.date.future(),
        created_at: faker.date.past(),
        worked_time: faker.random.number(),
        realizedHours: 'infos.............',
    }
 }

export function generateFakerWannaKamion(){
    return{
        title: 'From Wanna',
        expectedHourLabel: 'Heures prévues',
        expectedHour: faker.date.future(),
        realizedHoursLabel : 'Heures réalisé',
        realizedHours: faker.random.number(),
        deliveredLabel: 'Livré',
        delivered: faker.date.future(),
        lastUpdateLabel: 'Date de dernière mAJ',
        lastUpdate: faker.date.between(),
        deliveryDateLabel: 'Date de livraison',
        deliveryDate: faker.date.future(),
        pilotLabel: 'Pilote(s)',
        pilot: faker.name.findName(),
        projectChefLabel: 'Chef de kamion',
        projectChef: faker.name.findName(),
        modelLabel: 'modèle(s)',
        model: faker.lorem.words(),
        clientLabel: 'Nom du Client',
        client: faker.name.findName(),
        orderDateLabel: 'Date de commande',
        orderDate: faker.date.past(),
        projectDescriptionLabel: 'description du kamion',
        projectDescription: faker.lorem.sentence(),
        infoTitleLabel: 'Titre info',
        infoTitle: faker.lorem.words()


    }
}
export function generateFakerTrackingKamionArray(){
    return Array(10).fill(1).map((_, i) => {
        return {
            increase: faker.random.number(),
            name: faker.name.findName(),
            pHours: faker.random.number(),
            KHR: faker.random.number(),
            Kecart: faker.random.number(),
        }
    })
}
export function generateFakerTrackingKamionTitle(){
    return{
        title: 'Suivi des heures',
    }
}

export function generateFakerFreeInfoTitle(){
    return {
        title: 'Information Complémentaire'
    }
}
export function generateFakerFreeInfoArray(){
    return [
        {
            infoLabel: 'Information libre 1',
            info: 'info1'
        },
        {
            infoLabel: 'Information libre 2',
            info: 'info2'

        },
        {
            infoLabel: 'Information libre 3',
            info: 'info3',
        },
        {
            infoLabel: 'Information libre 4',
            info: 'info4',
        },
        {
            infoLabel: 'Information libre 5',
            info: 'info5',
        },
        {
            infoLabel: 'Information libre 6',
            info: 'info6',
        },
        {
            infoLabel: 'Information libre 7',
            info: 'info7',
        },
        {
            infoLabel: 'Information libre 8',
            info: 'info8',
        }

        ]
}
export function generateFakerFreeRelatedProjectsTitle(){
    return {
        title: 'kamions liés'
    }
}
export function generateFakerFreeRelatedProjectsArray(){
    return Array(2).fill(1).map((_, i) => {
    return{
            img: faker.image.image(),
            title: faker.lorem.words(),
            startDate: faker.date.future(),
            deliveredDate: faker.date.future(),
        }
    });


}