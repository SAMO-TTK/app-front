import faker from "faker"

export function generateFakerDataNotifications() {
    return Array(5).fill(1).map((_, i) => {
        return {
            kamion: "Projet " + (i+1),
            date: faker.date.past(),
            author: faker.name.findName(),
            details: faker.lorem.sentence(),
            photo: faker.image.image(),

        }
    })
}
