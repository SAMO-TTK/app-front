import faker from 'faker';

export function generateFakerChapter() {
    return {
        chapterName    : faker.lorem.word(),
        chatpterSection: faker.lorem.word(),
        chapterPart    : [
            {name: faker.lorem.word()},
            {name: faker.lorem.word()},
            {name: faker.lorem.word()},
        ]

    };
}
export function generateFakerChapterDocumentsArray() {
    return Array(6).fill(1).map((_, i) => {
        return {
            name: "Projet " + (i+1),
            creationDate: faker.date.past().toLocaleDateString(),
            author: faker.name.findName(),
            type: faker.lorem.word(),
            lastOpeningDate: faker.date.past(),

        }
    })
}
export function generateFakerChapterDebatesArray() {
    return Array(4).fill(1).map((_, i) => {
        return {
            emetteur: faker.name.findName(),
            waysNumber: faker.date.past().toLocaleDateString(),
            reference: faker.lorem.word(),
            dabateName: faker.lorem.words(),
            eventDate: faker.date.past(),
            status: faker.random.boolean(),


    }
    })
}
export function fakerChapter() {
    return {
        isForming: false,
        requirement: false,
        delay: true,
        funding: false,
        deadline: false,
        open: true,
    }
}