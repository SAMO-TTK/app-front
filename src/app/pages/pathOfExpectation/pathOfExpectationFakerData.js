import faker from 'faker';

export function generateFakerPathOhExpectationArray() {
    return Array(20).fill(1).map((_, i) => {
        return {
            waysNumber: faker.date.past(),
            reference: '{CONT_AV-00124-1}',
            transmitteur: 'TTK',
            lastModification: faker.date.past(),
            eventDate: faker.date.past(),
            elementName: faker.lorem.sentence(),
            status: faker.random.boolean(),
        };
    });

}

export function generateFakerPathOhExpectation() {
    return {
        title: 'Chemin des attentes',
        buttonModes: 'Modes',
        buttonActions: 'Actions'
    };


}