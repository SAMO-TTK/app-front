import {store} from '../config/store';

export default {
    path     : '/:idKamion',
    component: require('./Layout.vue').default,

    beforeEnter(to, from, next) {
        let idKamion = to.params.idKamion;
        if (idKamion) {
            if (idKamion === 'all') {
                store.dispatch(`unsetKamion`);
            } else {
                store.dispatch(`setKamion`, {idKamion});
            }
        }
        next();
    },

    alias    : '/all',
    children : [
        //#region - dashboard
        {
            path      : 'dashboard',
            name      : 'dashboard',
			beforeEnter(to, from, next){
				window.eventBus.$emit('load_content', true);
				next();
			},
            components: {
                content: require('./pages/Dashboard/dashboard.vue').default,
                aside  : require('./pages/Dashboard/aside').default
            }

        },
        //#endregion
        //#region - kamion
        {
            path      : 'kamion',
            name      : 'kamion',
            force       : true,
            redirect  : to => {
            	if (to.params.idKamion === 'all'){
            		return ('note');
				}else {
					return ('kamion/informations');
				}
			},
			beforeEnter(to, from, next){
				window.eventBus.$emit('load_content', true);
				next();
			},
            components: {
                aside  : require('./pages/Kamion/aside').default,
                content: require('./layout/AppContent.vue').default
            },

            children: [
                {
                    path      : 'informations',
                    name      : 'informations',
					beforeEnter(to, from, next){
						window.eventBus.$emit('load_content', true);
						next();
					},
                    components: {
                        content: require('./pages/Kamion/information/content.vue').default,
                    }

                },
                {
                    path      : 'instructions',
                    name      : 'instructions',
                    components: {
                        content: require('./pages/Kamion/instructions.vue').default,

                    }

                }

            ]
        },
		//#endregion
        //#region - documents
		{
			path      : 'documents',
			name      : 'documents',
			redirect      : 'documents/document/all',
			beforeEnter(to, from, next){
				window.eventBus.$emit('load_content', true);
				next();
			},
			components: {
				aside  : require('./pages/Documents/aside').default,
				content: require('./layout/AppContent.vue').default,
			},
			children : [
				{
					path      : 'document/:documentId(\\d+|all|All)?/:subDocumentId(\\d+)?',
					name      : 'document',
					beforeEnter(to, from, next){
						window.eventBus.$emit('load_content', true);
						next();
					},
					components: {
						content: require('./pages/Documents/content').default,
					},
				},
			]
		},
		//#endregion
        //#region - time
		{
			path		: 'time',
			name		: 'time',
			redirect	: 'time/out-time',

			components: {
				aside  : require('./pages/Time/aside').default,
				content: require('./layout/AppContent.vue').default,
			},
			children : [
				{
					path		: 'out-time',
					name		: 'out-time',
					components	:{
						content		: require('./pages/Time/out/content.vue').default
					}
				},
				{
					path		: 'metier-time-table',
					name		: 'metier-time-table',
					components	:{
						content		: require('./pages/Time/metier/table/content.vue').default
					}
				},
                {
                    path		: 'metier-time/:userId',
                    name		: 'metier-time-fiche',
                    components	:{
                        content		: require('./pages/Time/metier/single/content.vue').default
                    }
                },
				{
					path		: 'quality-time-table/:chiefId',
					name		: 'quality-time-table',
					components	:{
						content		: require('./pages/Time/quality/table/content.vue').default
					}
				},
                {
                    path		: 'quality-time/:chiefId/:userId',
                    name		: 'quality-time-fiche',
                    components	:{
                        content		: require('./pages/Time/quality/single/content.vue').default
                    }
                }
			]

		},
		//#endregion
        //#region - materials
		{
			path		: 'material',
			name		: 'material',
			redirect	: 'material/out-material',

			components: {
				aside  : require('./pages/Material/aside').default,
				content: require('./layout/AppContent.vue').default,
			},

			children : [
				{
					path		: 'out-material',
					name		: 'out-material',
					components	:{
						content		: require('./pages/Material/out/content.vue').default
					}
				}
			]

		},
		//#endregion
        //#region - pathOfExpectetion
        {
            path      : 'pathOfExpectetion',
            name      : 'pathOfExpectetion',
            redirect  : 'pathOfExpectetion/chapitre/all',
            components: {
                aside  : require('./pages/pathOfExpectation/aside.vue').default,
                content: require('./layout/AppContent.vue').default
            },
            children  : [
                {
					beforeEnter(to, from, next){
						window.eventBus.$emit('load_content', true);
						next();
					},
                    path      : 'chapitre/:chapterId(\\d+|all)?/:subChapterId(\\d+|all)?/:subChapterSecId(\\d+)?',
                    components: {
                        content: require('./pages/pathOfExpectation/table/content.vue').default,
                    },
                    children: [
                        {
							beforeEnter(to, from, next){
								window.eventBus.$emit('load_content', true);
								next();
							},
                            name: 'chapter-default',
                            path: '',
                            component: require('./pages/pathOfExpectation/table/component/ExpectetionElementTable.vue').default,
                        },
                        {
							beforeEnter(to, from, next){
								window.eventBus.$emit('load_content', true);
								next();
							},
                            name: 'chapter-validation',
                            path: 'validation',
                            component: require('./pages/pathOfExpectation/table/component/ExpectetionTableValidationMode.vue').default,
                        },
                        {
							beforeEnter(to, from, next){
								window.eventBus.$emit('load_content', true);
								next();
							},
                            name: 'chapter-quality',
                            path: 'quality',
                            component: require('./pages/pathOfExpectation/table/component/ExpectetionTableQualityMode.vue').default,
                        },
                    ]
                },

                {
					path      : 'chapitre/:chapterId/:subChapterId?/:subChapterSecId?/element/:elementId?/read?',
                    name      : 'element',
					beforeEnter(to, from, next){
						window.eventBus.$emit('load_content', true);
						next();
					},
                    components: {
                        content: require('./pages/pathOfExpectation/element/content.vue').default,
                    }

                }
            ]

        },
		//#endregion
        //#region - tickets
        {
            path      : 'tickets',
            name      : 'ticket',
            redirect: { name: 'out-ticket' },
			beforeEnter(to, from, next){
				window.eventBus.$emit('load_content', true);
				next();
			},
            components: {
                aside  : require('./pages/Ticket/aside.vue').default,
                content: require('./layout/AppContent.vue').default
            },
            children  : [
                {
                    path      : ':search?/:save?',
                    name      : 'out-ticket',
					beforeEnter(to, from, next){
						window.eventBus.$emit('load_content', true);
						next();
					},
                    components: {
                        content: require('./pages/Ticket/table/content.vue').default,
                    }
                },
				{
					path      : ':ticket_id/:notification_id?',
					name      : 'singleTicket',
					beforeEnter(to, from, next){
						window.eventBus.$emit('load_content', true);
						next();
					},
					components: {
						content: require('./pages/Ticket/single/content.vue').default,
					}

				}
            ]

        },
		//#endregion
        //#region - note
        {
            path		: 'note',
            name		: 'note',
            redirect	: 'note/out-note',

            components: {
                aside  : require('./pages/Note/aside').default,
                content: require('./layout/AppContent.vue').default,
            },

            children : [
                {
                    path		: 'out-note',
                    name		: 'out-note',
                    beforeEnter(to, from, next){
                        window.eventBus.$emit('load_content', true);
                        next();
                    },
                    components	:{
                        content		: require('./pages/Note/out/content.vue').default
                    },
                    props	:true
                }
            ]

        },
		//#endregion
        //#region - notifications
        {
            path      : 'notifications',
            name      : 'notifications',
			beforeEnter(to, from, next){
				window.eventBus.$emit('load_content', true);
				next();
			},
            components: {
                aside  : require('./pages/Notifications/aside').default,
                content: require('./pages/Notifications/notifications').default,
            }
        },
		//#endregion
    ]
};
