import { currentUserTypes } from './types';
export const currentUserMutations = {
	[currentUserTypes.CONNECT](state, event) {
		state.user      = event.user;
		state.token     = event.token;
		state.permissions     = event.permissions;
		state.connected = true;
	},

	[currentUserTypes.MFILES](state, event) {
		state.mfiles    = event.mfiles;
		state.mfilesConect = true;
	},

	[currentUserTypes.LOGIN_ERROR](state, { errors }) {
        state.loginErrors = errors;
    },

    [currentUserTypes.LOGOUT](state) {
        state.user      = null;
        state.token     = null;
        state.permissions = [];
        state.mfiles    = null;
        state.connected = false;
        state.mfilesConnected = false;
        state.loginErrors = null;
		console.log('logout => in',state)
    },
};
