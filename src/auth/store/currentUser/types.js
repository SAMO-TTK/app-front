export const currentUserTypes = Object.freeze({
    CONNECT    : 'CURRENT_USER_CONNECT',
	MFILES		: 'MFILES_CONNECT',
    LOGIN_ERROR: 'CURRENT_USER_LOGIN_ERROR',
    LOGOUT     : 'CURRENT_USER_LOGOUT'
});