export const currentUserGetters = {
    isConnected,
	isMfilesConnected,
    currentUser,
	currentPermissions:(state) => (id) => {
	return state.permissions;
},
    mfilesToken,
    loginErrors,
};


function isConnected(state) {
    return state.connected;
}

function currentUser(state) {
	return state.user;
}

function mfilesToken(state) {
	return state.mfiles;
}

function isMfilesConnected(state) {
	return state.mfiles != null;
}

function loginErrors(state) {
    return state.loginErrors
}
