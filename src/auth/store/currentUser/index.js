import { currentUserGetters } from './getters';
import { initialState } from './state';
import { currentUserActions } from './actions';
import { currentUserMutations } from './mutations';

export const currentUser = {
    state    : initialState,
    getters  : currentUserGetters,
    actions  : currentUserActions,
    mutations: currentUserMutations,
};