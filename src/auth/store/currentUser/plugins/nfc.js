import { store } from '../../../../config/store';

export function nfc() {
    const { NFC } = window.require('nfc-pcsc');
    const nfc = new NFC(); // optionally you can pass logger
    localStorage.setItem('is_admin', 'false');
    nfc.on('reader', reader => {

        reader.aid = 'F222222222';

        reader.on('card', card => {

            let user = store.getters.currentUser;

             if (localStorage.is_admin != 'true' && card.type == "TAG_ISO_14443_3") {

                if (user != null && card.uid == user.badge_id) {
                    store.dispatch('logout');
                }
                else {
                    store.dispatch('loginNFC', {cardid : card.uid});
                    store.dispatch('loginMfiles');
                }

             }

     	});

    });
    return nfc;
}
