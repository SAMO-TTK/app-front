import {currentUserTypes} from './types';
import {store} from '../../../config/store';


let lastToken = getToken();
let lastMfiles = getMfiles();

//#region - Store middleware for user storing
export function userMiddleware(store) {
	store.subscribe((({type}, state) => {
		const currentUserType = Object.values(currentUserTypes).includes(type);
		if (!currentUserType) return;
		const actualUser = state && state.currentUser && state.currentUser.user;
		localStorage.user = JSON.stringify(actualUser);
		lastToken = state.currentUser.token;
		localStorage.token = lastToken;
		lastMfiles = state.currentUser.mfiles;
		localStorage.mfiles = lastMfiles;
	}));
}

//#endregion


//#region - State loading from storage
const defaultState = {
	user: null,
	connected: false,
	admin: false,
	loginErrors: []
};

function getToken() {
	try {
		return JSON.parse(localStorage.token)
	} catch (e) {
		return null;
	}
}

function getMfiles() {
	try {
		return JSON.parse(localStorage.mfiles)
	} catch (e) {
		return null;
	}
}

export function getUserStateFromLocalStorage() {
	if (localStorage.user && localStorage.user !== 'undefined' && localStorage.user !== 'null') {

		try {
			const user = JSON.parse(localStorage.user);
			lastToken = JSON.parse(localStorage.token);
			lastMfiles = localStorage.mfiles;

			return {
				...defaultState,
				user,
				token: lastToken,
				mfiles: lastMfiles,
				mfilesConect : lastMfiles != null,
				connected: user != null,
				admin: user != null,
			};
		} catch (e) {
			console.error('A user was found but unable to load it', e);
		}
	}
	return defaultState;
}

//#endregion


//#region Http Interceptor to add token to all request, and redirect on unauthenticated response
export function userTokenInterceptor(request, next) {

	if (request.url.match(/mfiles/)) {
		const actualMfiles = request.headers.get('X-Authentication');
		if (actualMfiles || lastMfiles) {
			request.headers.set('X-Authentication', actualMfiles || `${lastMfiles}`);
		}
		next(checksetmfiles);
	}
	else {
		const actualToken = request.headers.get('Authorization');
		if (actualToken || lastToken) {
			request.headers.set('Authorization', actualToken || `Bearer ${lastToken}`);
		}
		next(redirectOnAuthErrors);
	}

}

const authErrorsStatuses = [401, 403];

function redirectOnAuthErrors(response) {
	const isAnAuthError = response.url !== `${process.env.API}/login` && authErrorsStatuses.includes(response.status);

	if (isAnAuthError) {
		store.dispatch('logout', {invalidToken: true});
	}
}

function checksetmfiles(response){
	const isAnAuthError = response.url !== `${process.env.API}/login` && authErrorsStatuses.includes(response.status);

	if (isAnAuthError) {
		store.dispatch('unsetMfiles', {invalidToken: true});
	}
}
//#endregion
