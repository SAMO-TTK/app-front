import Vue from 'vue';

import { currentUserTypes } from './types';
import { Router }   from '../../../config/router';
import chooseKamion from '../../../app/pages/chooseKamion/chooseKamion';

import JWTDecode    from 'jwt-decode';
import {$openModal, $closeModal} from '../../../common/components/AppModal/modalBus';

export const currentUserActions = {
	login({ commit}, { password, username }) {

		const electron = window.require('electron');
		const session = electron.remote.session;
		const ses = session.fromPartition('persist:name');

		Vue.http.post(`${process.env.API}/login`, {
			username,
			password
		}).then(apiResponse => {
			const token = apiResponse.body.token;
			Vue.http.get(`${process.env.API}/users/${JWTDecode(token).sub}?embed=roles.permissions`, {
				headers: {
					'Authorization': `Bearer ${token}`
				},
			}).then(rep => {
				const user = rep.body.data;
				const permissions = [];
				user.roles.forEach(role => {
					role.permissions.forEach(item => {
						if(permissions.indexOf(item.id) === -1)
							permissions.push(item.id);
					})
				})
				console.log('=>',permissions)
				commit(currentUserTypes.CONNECT, { user, token, permissions});
				return $openModal(chooseKamion, {size: 'full', closable: false});
			})
		}).catch(response => {
			return window.eventBus.$message.error('Mot de passe invalide');
		});
	},
	loginMfiles({ commit}){
		Vue.http.post(
			`${process.env.MFILES}/server/authenticationtokens`,
			JSON.stringify({
				Username: "ttkhubitb",
				Password: "ttkhubitb",
				VaultGuid: "{7061608C-319D-43C7-9017-6D940C3731C7}"
			})).then((mfilesResponse) => {
			const mfiles = mfilesResponse.body.Value.replace('"', '');
			commit(currentUserTypes.MFILES, { mfiles});
		}).catch(response => {
			const mfiles = null;
			commit(currentUserTypes.MFILES, { mfiles});
		});
	},
	unsetMfiles({commit}){
		commit(currentUserTypes.MFILES, {mfiles: null});
	},
	loginNFC({ commit, state }, { cardid }) {

		Vue.http.post(`${process.env.API}/nfc`, {
			badge_id : cardid,
        }).then(apiResponse => {
            const token = apiResponse.body.token;
            Vue.http.get(`${process.env.API}/users/${JWTDecode(token).sub}?embed=roles.permissions`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                },
            }).then(rep => {
                const user = rep.body.data;
                const permissions = [];
                user.roles.forEach(role => {
                    role.permissions.forEach(item => {
                        if(permissions.indexOf(item.id) === -1)
                            permissions.push(item.id);
                    })
                })
                console.log('=>',permissions)
                commit(currentUserTypes.CONNECT, { user, token, permissions});
                return $openModal(chooseKamion, {size: 'full', closable: false});
            })
        }).catch(response => {
            return window.eventBus.$message.error('Carte inconnu');
        });
	},

	logout({ commit, state }, {invalidToken} = {}) {
		console.log('logaout => set')
		if (!invalidToken) {
			Vue.http.get(`${process.env.API}/logout`,{
				headers: {
					'Authorization': `Bearer ${state.token}`
				}
			});
		}
		$closeModal(chooseKamion);
		commit(currentUserTypes.LOGOUT);
		Router.push('/auth/login');
	}
};
