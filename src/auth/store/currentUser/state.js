import { getUserStateFromLocalStorage } from './plugins';

export const initialState = getUserStateFromLocalStorage();