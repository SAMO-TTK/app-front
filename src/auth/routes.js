export default {
    path: '/auth',
    component: require('./pages/Skeleton.vue').default,

    children: [
        {
            path: 'login',
            component: require('./pages/Login.vue').default

        }, {
            path: 'reset-password',
            component: require('./pages/ResetPassword.vue').default

        }
    ]
}
