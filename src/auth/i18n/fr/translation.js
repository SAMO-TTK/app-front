export default {
    login: {
        username: 'Nom d\'utilisateur',
        password: 'Mot de passe',
        logout  : 'Déconnection',
        login   : 'Se connecter',
        by      : 'Se connecter',
        brand   : 'toutenkamion',
        instructions: 'Badgez pour vous connecter'
    }
};
