export default {
    login: {
        logout  : 'Logout',
        username: 'Username',
        password: 'Password',
        login   : 'Log in',
        by      : 'Designed by ',
        brand   : 'Toutenkamion',
        instructions: 'Badge to connect'

    }
};