import { store } from '../config/store';

export const AuthFilters = {
    isAuthenticated(to, from, next) {
        if (to.path === '/auth/login' || store.state.currentUser.connected) {
            next();

        } else {
            next({ path: '/auth/login' });
        }
    }
}
