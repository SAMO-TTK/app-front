import VueI18n from 'vue-i18n';
import Vue from 'vue';
// Fr locales
import elementFrLocale from 'element-ui/lib/locale/lang/fr';
import authFrLocale from '../auth/i18n/fr/translation.js';
import appFrLocale from '../app/i18n/fr/translation.js';
import backofficeFrLocale from '../admin/i18n/fr/translation.js';
// En Locales
import elementEnLocale from 'element-ui/lib/locale/lang/en';
import authEnLocale from '../auth/i18n/en/translation.js';
import appEnLocale from '../app/i18n/en/translation.js';
import backofficeEnLocale from '../admin/i18n/en/translation.js';


Vue.use(VueI18n);

export const i18n = new VueI18n({
    locale  : 'fr',
    messages: {
        en: {
            ...elementEnLocale,
            ...authEnLocale,
            ...appEnLocale,
            ...backofficeEnLocale,
        },
        fr: {
            ...elementFrLocale,
            ...authFrLocale,
            ...appFrLocale,
            ...backofficeFrLocale,
        },
    }
});
