import Vue              from 'vue';
import VueRouter        from 'vue-router';
import { AuthFilters }  from '../auth/filters';



Vue.use(VueRouter);

export const Router = new VueRouter({
	// base: '/hub',
    routes: [
        {
            path: '/',
            redirect: '/auth/login',
        },
        require('../app/routes').default,
        require('../auth/routes').default,
        require('../admin/routes').default,
    ],

    mode: 'history',
    abstract: true,
});

Router.beforeEach(AuthFilters.isAuthenticated);
