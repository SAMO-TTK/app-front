import Vuex from 'vuex';
import Vue from 'vue';
import { currentUser } from '../auth/store/currentUser';
import { kamion } from '../app/store/kamion';
import { debate } from '../app/store/debates';
import { mfiles } from '../app/store/mFiles.js';
import { userMiddleware } from '../auth/store/currentUser/plugins';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export const store = new Vuex.Store({
    modules: {
        currentUser,
        kamion,
        debate: debate,
        mfiles
    },
    strict : debug,
    plugins: [
        userMiddleware
    ]
});
