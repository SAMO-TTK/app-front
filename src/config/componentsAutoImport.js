import Vue from 'vue';

/**
 * Auto import of component anywhere in the project
 */
// const requireComponent = require.context('../', true, /.*\/component\/.*(\.vue|\.component\.js)$/);
//
// requireComponent.keys().map(key => {
//     const name = key.replace(/.*\/([^/]+)(?:\.vue|\.component\.js)/, '$1');
//
//     const { __esModule, ...module } = requireComponent(key);
//
//     Vue.component(name, __esModule ? module.default : module);
// });

/**
 * Auto import of filters anywhere in the project
 */
const requireFilter = require.context('../', true, /.*\/filters\/.*(\.js)$/);

requireFilter.keys().map(key => {
    const name = key.replace(/.*\/([^/]+)(?:\.js)/, '$1');

    const { __esModule, ...module } = requireFilter(key);

    Vue.filter(name, __esModule ? module.default : module);
});

/**
 * Auto import of directives anywhere in the project
 */
// const requireDirective = require.context('../', true, /.*\/directives\/.*(\.js)$/);
//
// requireDirective.keys().map(key => {
//     const name = key.replace(/.*\/([^/]+)(?:\.js)/, '$1');
//
//     const { __esModule, ...module } = requireDirective(key);
//
//     Vue.directive(name, __esModule ? module.default : module);
// });
