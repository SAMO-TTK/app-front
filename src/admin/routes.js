export default {
    path: '/admin',
    component: require('./Layout.vue').default,

    children: [
        {
            path: '',
            redirect: {name: 'users'}
        },

        //#region - Users
		{
            path: '/utilisateurs/:id/modifier',
            name: 'user_edit',
            component: require('./pages/Users/Edit.vue').default
        },
        //#endregion

        //#region - Roles
        {
            path: '/roles/:id/modifier',
            name: 'role_edit',
            component: require('./pages/Users/Edit.vue').default
        },
        //#endregion

        //#region - Groups
        {
            path: '/groupes/:id/modifier',
            name: 'group_edit',
            component: require('./pages/Users/Edit.vue').default
        },
        //#endregion

        //#region - Metas
        {
            path: '/metas/:id/modifier',
            name: 'meta_edit',
            component: require('./pages/Users/Edit.vue').default
        },
        //#endregion


    ]

}
