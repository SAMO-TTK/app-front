export default {
    admin: {
        nav: {
            back: 'Retour TTK',
            users: 'Utilisateurs',
            roles: 'Rôles',
            groups: 'Groupes',
            metas: 'Metas',
            logs: 'Logs'
        },
        forms: {
            validate: {
                required: 'Ce champ est obligatoire.',
                email   : 'Ce champ doit être un email valide.'
            },
            ok    : 'Valider',
            save  : 'Enregistrer',
            cancel: 'Annuler',
        },
        users: {
            create: {
                title     : 'Ajouter un utilisateur',
                first_name: 'Prénom',
                last_name : 'Nom',
                email     : 'Email',
                password  : 'Mot de passe',
				username  : 'Nom d\'utilisateur',
            }
        },
        logs: {
        },
    }
}
