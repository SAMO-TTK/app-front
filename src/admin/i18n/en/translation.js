export default {
    admin: {
        nav: {
            back: 'Back to TTK',
            users: 'Users',
            roles: 'Roles',
            groups: 'Groups',
            metas: 'Metas',
            logs: 'Logs'
        },
        users: {
            list: {
                title: 'Users'
            }
        },
    }
}
