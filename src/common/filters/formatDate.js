import moment from 'moment';

export default function formatDate(value, format = null) {
    moment.locale('fr');
    if(value) {
        if (format)
            return moment(String(value)).format(format);
        else
            return moment(String(value)).format('DD/MM/YYYY hh:mm');

    }
}
