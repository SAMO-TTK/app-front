import moment from 'moment';
moment.locale('fr');

export default function dateFormat(string, dateFormat) {
return  moment(string).format(dateFormat);
}
