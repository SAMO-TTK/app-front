
export default function booleanFilter(value, isTrue, isFalse){
    value = !!value;
    return  value ? isTrue : isFalse;
}