
export default function hyphenate(string, numberCharactere ){
    if (string.length > numberCharactere) {
        string = string.substr(0,numberCharactere)+'...';
    }

    return  string;
}
