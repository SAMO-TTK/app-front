
export default function splitOn(string, charactere ){
    return string.toLowerCase().split(charactere).pop();
}