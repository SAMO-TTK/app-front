import moment from 'moment';

export default function fromNow(value) {
    moment.locale('fr');
    if(value) {
        return moment(String(value)).fromNow();
    }
}
