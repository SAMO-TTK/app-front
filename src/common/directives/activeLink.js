import {Router} from '../../config/router';

const activeClass = 'active-link';

export function matchRoute(route) {
    const toMatch = {...route};
    const current = Router.currentRoute;

    //let resolvedToMatch = Router.resolve(toMatch, current);
    //return current.fullPath.startsWith(resolvedToMatch.href); --> Marche pas
    //return current.fullPath.startsWith(resolvedToMatch.resolved.fullPath); --> Marche !

    Router.resolve(toMatch, current); // --> on comprend pas, mais il y en a besoin visiblement... peut-être... enfin surement...
    return current.fullPath.startsWith(toMatch.path); // --> Pragmatique
}

export default function activeLink(el, binding) {
    let matches = matchRoute(binding.value);
    if (matches) {
        el.classList.add(activeClass);
    } else {
        el.classList.remove(activeClass);
    }
}
