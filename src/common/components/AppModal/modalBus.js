import Vue from 'vue';

/**
 * Way to communicate between global event and modal component
 */
export const modalEventBus = new Vue();

/**
 * Way to launch an open modal event globaly.
 * @param Component to be displayed in the modal
 * @param params el-dialog props
 * @returns {Promise} then(ok, cancel).catch(cancel)
 */
export const $openModal = (Component, params) => {
    return new Promise((resolve, reject) => {
        modalEventBus.$emit('open-modal', Component, params, resolve, reject);
    });
};

/**
 * Way to close modal globaly
 * @returns {Promise}
 */
export const $closeModal = () => {
    return new Promise((resolve, reject) => {
        modalEventBus.$emit('close-modal', resolve, reject);
    });
};

/**
 * Way to cancel modal globaly
 * @returns {Promise}
 */
export const $cancelModal = () => {
    return new Promise((resolve, reject) => {
        modalEventBus.$emit('cancel-modal', resolve, reject);
    });
};

/**
 * Make all component in the app able to use this.$openModal/$closeModal/$cancelModal
 */
Vue.mixin({
    methods: { $openModal, $cancelModal, $closeModal }
});
