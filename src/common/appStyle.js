import variables from '!!css-variables-loader!./theme/app-variables.css';
import Vue from 'vue';

const toUpperCase = (s) => s.toUpperCase();
const ignoreFirstParam = (fn) => (_, ...args) => fn(...args);
const secondParamToUpperCase = ignoreFirstParam(toUpperCase);

export const appStyle = Object.keys(variables)
    .map((key) => {
        const calculatedKey = key
            .replace(/^--/, '')
            .replace(/-(.)/gi, secondParamToUpperCase);
        return { [calculatedKey]: variables[ key ] };
    }).reduce((acc, v) => Object.assign(acc, v), {});

/**
 * Inject css variables as $appStyle in component
 */
Vue.mixin({
    computed: {
        $appStyle: () => require('./appStyle').appStyle
    }
});