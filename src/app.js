import Vue from 'vue';
import _ from 'lodash';
import App from './config/App.vue';
import moment from 'moment';
import VueResource from 'vue-resource';
import Vue2Filters from 'vue2-filters';
import { store } from './config/store';

import { i18n } from './config/i18n';
import Print from 'vue-print-nb';
import { Router } from './config/router';
import Element from 'element-ui';
import './common/theme/element-ui/index.css';
import './common/theme/theme-overload.css';
import './common/theme/global.css';
import './common/appStyle';
import './config/componentsAutoImport';
import 'normalize.css';
import 'babel-polyfill';
import { userTokenInterceptor } from './auth/store/currentUser/plugins';
import IdleVue from 'idle-vue'
import {mapActions} from 'vuex'

/**
 * Configuration of element ans his locale system
 * to use the same version of vue-i18n as the project
 */
Vue.use(Element, {
    i18n: key => i18n.t(key)
});

Vue.use(VueResource);
Vue.use(Print);
Vue.use(Vue2Filters);

Vue.http.interceptors.push(userTokenInterceptor);

Vue.http.options.emulateJSON = true;

if (process.env.NODE_ENV === 'production' && process.env.NFC) {
    const { nfc }  = require('./auth/store/currentUser/plugins/nfc');
    nfc();
}

localStorage.clear();

/**
 * App initialization
 */
window.eventBus = new Vue();
Vue.use(IdleVue, {
	eventEmitter: window.eventBus,
	idleTime: 1000 * 60 * 10
})
new Vue({
    store : store,
    router: Router,
    i18n  : i18n,
    render(h) {
        return (<App/>);
    },
	computed:{
		...mapActions(['logout'])
	},

	onIdle() {
		this.logout();
	},
	onActive() {
	}
}).$mount('#app');
